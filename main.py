import os
import time

import src.utils.settings
from src.utils.constants import (
    MODULE_DESCRIPTION,
    SHOW_DOCS_ENVIRONMENT,
)
from src.utils._logger import logger
from src.utils.sentry import sentry_kr

import uvicorn
from fastapi import FastAPI, Request, File, Form, UploadFile, status, HTTPException
from fastapi.middleware.cors import CORSMiddleware

# from starlette.responses import StreamingResponse
# from io import BytesIO
# from PIL import Image

# import protobuf_generated_files.protobuf.image_pb2 as image_pb2


app_configs = {"title": MODULE_DESCRIPTION}

if os.getenv("PY_ENV", "PRODUCTION").upper() not in SHOW_DOCS_ENVIRONMENT:
    app_configs["openapi_url"] = None  # set url for docs as null

app = FastAPI(**app_configs)

int(os.environ.get("PORT", "8000"))

origins = [
    "http://localhost",
    "http://localhost:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST"],
    allow_headers=["*"],
)


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    logger.info(f"Request took {process_time} seconds")
    return response


@app.get("/live")
async def is_app_live():
    """
    Checks if the app is alive or not.

    Returns:
        tuple: The output message and status code.
    """
    return "OK", 200


@app.get("/ready")
async def is_app_ready():
    """
    Checks if the app is ready to accept requests.

    Returns:
        tuple: The output message and status code.
    """
    return "OK", 200


# @app.get("/image/{image_name}")
# async def send_image(image_name: str, directory="images", extension="jpg"):

#     if not os.path.exists(f"{directory}/{image_name}.{extension}"):
#         raise HTTPException(status_code=404, detail="Image not found")
#     image_protobuf = image_pb2.image()
#     pil_frame = Image.open(f"{directory}/{image_name}.{extension}")
#     img_stream = BytesIO()
#     pil_frame.save(img_stream, format="jpeg", optimize=True)
#     img_stream.seek(0)

#     image_protobuf.image_data = img_stream.read()

#     return StreamingResponse(
#         BytesIO(image_protobuf.SerializeToString()), media_type="binary"
#     )

if __name__ == "__main__":
    uvicorn.run(
        app, host="0.0.0.0", port=int(os.environ.get("PORT", "8000")), reload=False
    )
