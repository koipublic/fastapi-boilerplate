FROM public.ecr.aws/koireader/base-images:builder.ubuntu20.python311.pulsar2.9 as poetry

WORKDIR /app

COPY poetry.lock pyproject.toml ./

RUN poetry install --without dev --no-root && \
    rm -rf $POETRY_CACHE_DIR
    # CC="cc -mavx2" pip install -U --no-cache-dir --force-reinstall pillow-simd==9.0.0.post1


FROM public.ecr.aws/koireader/base-images:release.ubuntu20.python311 as runtime

WORKDIR /app

ENV VENV_PATH=/app/.venv \
    PATH="/app/.venv/bin:$PATH"

ARG OTEL_SERVICE_NAME
ARG OTEL_SERVICE_VERSION

ENV OTEL_SERVICE_NAME=${OTEL_SERVICE_NAME} \
    OTEL_SERVICE_VERSION=${OTEL_SERVICE_VERSION} \
    OTEL_RESOURCE_ATTRIBUTES="service.version=$OTEL_SERVICE_VERSION" \
    OTEL_PYTHON_LOG_CORRELATION=true \
    OTEL_EXPORTER_OTLP_PROTOCOL="http/protobuf" \
    OTEL_PYTHON_EXCLUDED_URLS="ping,live,ready" \
    OTEL_INSTRUMENTATION_HTTP_CAPTURE_HEADERS_SANITIZE_FIELDS=".*session.*,set-cookie,x-apikey"

# Uncomment for debugging OpenTelemetry using console
# ENV OTEL_TRACES_EXPORTER="console" \
#     OTEL_METRICS_EXPORTER="console" \
#     OTEL_LOGS_EXPORTER="console"

COPY --chown=koireader:koireader --from=poetry $VENV_PATH $VENV_PATH
COPY --chown=koireader:koireader . ./

USER root

RUN chown -R koireader:koireader /app

USER koireader

EXPOSE 8000

ENTRYPOINT [ "/tini", "--" ]

CMD [ "opentelemetry-instrument", "python", "-m", "main" ]
