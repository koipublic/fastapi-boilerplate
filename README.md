
# FastAPI Boilerplate

## After your first clone, follow these steps.

1. Download and unzip latest Protobuf compiler by running following commands
```
wget -P protobuf_generated_files "https://github.com/protocolbuffers/protobuf/releases/download/v21.2/protoc-21.2-linux-x86_64.zip"
unzip protobuf_generated_files/protoc-21.2-linux-x86_64.zip -d protobuf_generated_files/protoc-21.2-linux-x86_64
rm protobuf_generated_files/protoc-21.2-linux-x86_64.zip
```
2. Run `protobuf_generated_files/protoc-21.2-linux-x86_64/bin/protoc --python_out=protobuf_generated_files protobuf/image.proto`
3. Install dependencies using poetry. Run: `poetry install`
4. Run API using command: `poetry run python main.py`
5. Run `poetry run python test/get_files.py` to test API
