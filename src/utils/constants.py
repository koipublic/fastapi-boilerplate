import os
import tomllib

with open("pyproject.toml", "r") as file:
    toml_data = tomllib.loads(file.read())
    MODULE_NAME = os.getenv("OTEL_SERVICE_NAME", toml_data["tool"]["poetry"]["name"])
    MODULE_VERSION = os.getenv(
        "OTEL_SERVICE_VERSION", toml_data["tool"]["poetry"]["version"]
    )
    MODULE_DESCRIPTION = toml_data["tool"]["poetry"]["description"]

SHOW_DOCS_ENVIRONMENT = {
    "LOCAL_DEVELOPMENT",
    "DEVELOPMENT",
}  # explicit list of allowed envs where we will show /docs

# NOTE : PY_ENV is set to "PRODUCTION" by default.
# PRODUCTION : Actual Production - Koireader - True PROD
# DEVELOPMENT : Production with Dev Clustering Environment Changes - KoiVentures - DEV - DEV_SERVER
# LOCAL_DEVELOPMENT : Local User Changes, Saving Local Files
