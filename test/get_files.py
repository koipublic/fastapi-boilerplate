import sys
import os

sys.path.append(os.getcwd())
from io import BytesIO
from PIL import Image
import protobuf_generated_files.protobuf.image_pb2 as image_pb2
import numpy as np
import requests
import shutil
import os
from src.utils._logger import logger


def download_binary(url, binary_file_directory_path):
    local_filename = url.split("/")[-1]
    with requests.get(url, stream=True) as r:
        with open(f"{binary_file_directory_path}/{local_filename}", "wb") as f:
            shutil.copyfileobj(r.raw, f)

    return f"{binary_file_directory_path}/{local_filename}"


def get_image(url, image_name, image_directory, binary_file_directory_path="temp/"):
    if not os.path.exists(binary_file_directory_path):
        os.makedirs(binary_file_directory_path)
    if not os.path.exists(image_directory):
        os.makedirs(image_directory)
    binary_file_path = download_binary(
        f"{url}/{image_name}", binary_file_directory_path
    )
    image_protobuf = image_pb2.image()
    with open(binary_file_path, "rb") as f:
        image_protobuf.ParseFromString(f.read())
    image = np.array(Image.open(BytesIO(image_protobuf.image_data)))

    im = Image.fromarray(image)
    im.save(f"{image_directory}/{image_name}.jpg")
    logger.debug(f"Saved {image_name} in {image_directory}")


get_image("http://0.0.0.0:8000/image", "6703294", image_directory="output")
