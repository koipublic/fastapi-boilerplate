# builtin imports
import os
import sys

# pypi imports
from loguru import logger
import src.utils.settings
from src.utils.constants import (
    MODULE_NAME,
)


def set_logger(is_enable=True, log_level=os.getenv("LOG_LEVEL", "WARNING")):
    """
    is_enable: boolean, default - False
    log_level: takes in all loguru log levels, will
        look for LOG_LEVEL environment variable else set to be default WARNING
    """
    if is_enable:
        logger.remove()
        logger.add(sys.stdout, level=log_level)

        if os.getenv("DEV_LOGS") == "1":
            logfile = f"logs.log"
            logger.add(logfile, level=log_level)
            setattr(logger, "sink", logfile)

        logger.enable(MODULE_NAME)
    else:
        logger.disable(MODULE_NAME)


if os.getenv("PY_ENV", "PRODUCTION").upper() == "PRODUCTION":
    set_logger()
else:
    set_logger(log_level="DEBUG")
