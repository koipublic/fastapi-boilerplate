# builtin imports
import os

from sentry_sdk.integrations.starlette import StarletteIntegration
from sentry_sdk.integrations.fastapi import FastApiIntegration
from sentry_sdk import capture_exception, capture_message, set_context
from sentry_sdk import init as sentry_init

from src.utils._logger import logger
import src.utils.settings
from src.utils.constants import (
    MODULE_VERSION,
)


class SentrySetup:
    """
    Setting up the Sentry SDK for the Application.
    """

    def __init__(self) -> None:
        os.environ["SENTRY_RELEASE"] = MODULE_VERSION
        sentry_init(
            dsn=os.getenv("SENTRY_DSN", None),
            max_breadcrumbs=90,
            debug=False,
            attach_stacktrace=True,
            integrations=[
                StarletteIntegration(),
                FastApiIntegration(),
            ],
            traces_sample_rate=0.05,
            instrumenter="otel",
        )

    @staticmethod
    def setContext(context_name: str, data: dict) -> None:
        set_context(context_name, data)

    @staticmethod
    def capExc(err: Exception):
        capture_exception(err)
        if os.getenv("PY_ENV", "PRODUCTION") == "PRODUCTION":
            logger.error(err)
        else:
            logger.exception(str(err), backtrace=True, diagnose=True)

    @staticmethod
    def capMsg(msg: str) -> None:
        logger.warning(f"Sentry capmsg : {msg}")
        capture_message(msg)


sentry_kr = SentrySetup()
